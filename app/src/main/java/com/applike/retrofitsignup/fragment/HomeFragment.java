package com.applike.retrofitsignup.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applike.retrofitsignup.R;
import com.applike.retrofitsignup.storage.SprefManager;

public class HomeFragment extends Fragment {

    private TextView textViewEmail, textViewName, textViewSchool;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textViewEmail = view.findViewById(R.id.textViewEmail);
        textViewName = view.findViewById(R.id.textViewName);
        textViewSchool = view.findViewById(R.id.textViewSchool);

        textViewEmail.setText(SprefManager.getInstance(getActivity()).getUser().getEmail());
        textViewName.setText(SprefManager.getInstance(getActivity()).getUser().getName());
        textViewSchool.setText(SprefManager.getInstance(getActivity()).getUser().getSchool());

    }
}
