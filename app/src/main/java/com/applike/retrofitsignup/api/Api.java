package com.applike.retrofitsignup.api;

import com.applike.retrofitsignup.models.DefaultResponse;
import com.applike.retrofitsignup.models.LoginResponse;
import com.applike.retrofitsignup.models.UsersResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {

    @FormUrlEncoded
    @POST("createUser")
    Call<DefaultResponse> createUser(
            @Field("email") String email,
            @Field("password") String password,
            @Field("name") String name,
            @Field("school") String school);

    @FormUrlEncoded
    @POST("userLogin")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password);

    @GET("allUsers")
    Call<UsersResponse> getUsers();

    @FormUrlEncoded
    @PUT("updateUser/{id}")
    Call<LoginResponse> updateUser(
            @Path("id") int id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("school") String school);

    @FormUrlEncoded
    @PUT("updatePassword/{id}")
    Call<DefaultResponse> updatePassword(
            @Field("currentpassword") String currentPassword,
            @Field("newpassword") String newPassword,
            @Field("email") String email);

    @DELETE("deleteUser/{id}")
    Call<DefaultResponse> deleteUser(@Path("id") int id);
}
