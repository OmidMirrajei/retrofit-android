package com.applike.retrofitsignup.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.applike.retrofitsignup.R;
import com.applike.retrofitsignup.fragment.HomeFragment;
import com.applike.retrofitsignup.fragment.SettingFragment;
import com.applike.retrofitsignup.fragment.UserFragment;
import com.applike.retrofitsignup.models.User;
import com.applike.retrofitsignup.storage.SprefManager;

public class ProfileActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        BottomNavigationView navigationView = findViewById(R.id.bottomNavigationView);
        navigationView.setOnNavigationItemSelectedListener(this);

        displayFragment(new HomeFragment());
    }

    private void displayFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.containerProfile, fragment)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!SprefManager.getInstance(this).isLoggedIn()) {
            Intent intent = new Intent(this, SignUpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.menu_home:
                fragment = new HomeFragment();
                break;

            case R.id.menu_users:
                fragment = new UserFragment();
                break;

            case R.id.menu_settings:
                fragment = new SettingFragment();
                break;
        }
        if(fragment != null) {
            displayFragment(fragment);
        }

        return false;
    }
}
